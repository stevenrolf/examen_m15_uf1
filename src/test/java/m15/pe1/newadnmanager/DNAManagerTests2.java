/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author tarda
 */
public class DNAManagerTests2 {
    
    String dnaSequence;
    ADN_Manager dnaFunct;
    
    public DNAManagerTests2() {
        dnaSequence = "GATATGC";
        dnaFunct = new ADN_Manager();
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.

    @Test
    public void testMinLetterDNA_COneTime() {
        dnaSequence = "GATATGC";
        String expected = "C";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result,expected);
    }
    
    @Test
    public void testMinLetterDNA_T_Twice() {
        dnaSequence = "CCGATACATGAC";
        String expected = "T";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result,expected);
    }
    
    @Test
    public void testMaxLetterDNA_C() {
        dnaSequence = "CCGATACATGACC";
        String expected = "C";
        String result = dnaFunct.maxLetter(dnaSequence);
        assertEquals(result,expected);
    }
    
    /*
    Se realiza test maximo de letras de adenina
    */
    
    @Test
    public void testMaxLetterDNA_A() {
        dnaSequence = "ACGATACATGAAA";
        String expected = "A";
        String result = dnaFunct.maxLetter(dnaSequence);
        assertEquals(result,expected);
    }
    
    /*
    Se realiza test de numero de adeninas
    */
    
    @Test
    public void testnumAdenines_A() {
        dnaSequence = "ACGATACATGAAA";
        int expected = 7;
        int result = dnaFunct.numAdenines(dnaSequence);
        assertEquals(result,expected);
    }
    
    /*
    La primera longitud es la A = 53
    */
    @Test
    public void testPorcentajes() {
        dnaSequence = "ACGATACATGAAA";
        int expected[] = {53,15,15,15};
        int result[] = dnaFunct.porcentajes(dnaSequence);
        assertArrayEquals(expected, result);
    }
    
    
//    TODO Pending DNA full integrity validation.
//    @Test
//    public void testMinLetterDNAInvalidLetterD() {
//        dnaSequence = "GATA";
//        String expected = "C";
//        String result = dnaFunct.minLetter(dnaSequence);
//        assertEquals(result,expected);
//    }
}

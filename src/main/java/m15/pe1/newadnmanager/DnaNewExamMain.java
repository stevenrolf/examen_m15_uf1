package m15.pe1.newadnmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class DnaNewExamMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        DnaNewExamMain myApp = new DnaNewExamMain();
        
        myApp.run();
    }

    private static final String TOTAL_DE_ = "Total de ";

    
    /**
     * Function that runs the app
     */
    private void run() {
        System.out.println("Introduce la cadena que quieres desarrollar: ");
        ReaderTerminal text = new ReaderTerminal();
        String dnaSequence = text.reader();
        
        int option = 0;
        ADN_Manager cadenaADN = new ADN_Manager();
//        ADNFileReader file = new ADNFileReader();
//        ArrayList<String> dnaSequence_list = file.readSequence("src/main/java/m15/pe1/newadnmanager/dnaSequence.txt");
//        String dnaSequence = String.join("", dnaSequence_list);
//        dnaSequence = dnaSequence.toUpperCase();

        do {
            menu();
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();

            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    System.out.println("**Donar la volta " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(cadenaADN.invertADN(dnaSequence));
                    break;
                case 2:
                    System.out.println("**Trobar la base més repetida " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(cadenaADN.maxLetter(dnaSequence));
                    break;
                case 3:
                    System.out.println("**Trobar la base menys repetida " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(cadenaADN.minLetter(dnaSequence));
                    break;
                case 4:
                    System.out.println("**Fer recompte de bases " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(TOTAL_DE_ + "A: " + cadenaADN.numAdenines(dnaSequence));
                    System.out.println(TOTAL_DE_ + "C: " + cadenaADN.numCitosines(dnaSequence));
                    System.out.println(TOTAL_DE_ + "G: " + cadenaADN.numGuanines(dnaSequence));
                    System.out.println(TOTAL_DE_ + "T: " + cadenaADN.numTimines(dnaSequence));
                    break;
                case 5:
                    System.out.println("**Fer recompte de bases per percentatge " + dnaSequence + "**");
                    System.out.println("");
                    int totalADN[] = cadenaADN.porcentajes(dnaSequence);
                    System.out.println(TOTAL_DE_ + "A: " + totalADN[0]+"%");
                    System.out.println(TOTAL_DE_ + "G: " + totalADN[1]+"%");
                    System.out.println(TOTAL_DE_ + "T: " + totalADN[2]+"%");
                    System.out.println(TOTAL_DE_ + "C: " + totalADN[3]+"%");
                    break;
                default:
                    System.out.println("La opció seleccionada no és válida.");
                    break;
            }
        } while (option != 0);
    }

    /*
    Se realiza un metodo para el menu
    */
    private void menu() {
        System.out.println("\n Tria una opció:");
        System.out.println("0.-Sortir");
        System.out.println("1.-Donar la volta\n"
                + "2.- Trobar la base més repetida,\n"
                + "3.- Trobar la base menys repetida\n"
                + "4.- Fer recompte de bases\n"
                + "5.- Fer recompte de bases per porcentatge\n");
        System.out.print("\nOpció: ");
    }
    
    /*
    Se realiza las secuencias
    */

    private void ShowSecuences(String dnaSequence, String dnaSubSequence) {
        System.out.println("DNA sequence: " + dnaSequence);
        System.out.println("DNA subSequence: " + dnaSubSequence);
    }
}
